import pandas as pd
import numpy as np
import math
from nltk.corpus import stopwords
import re

import pymorphy2

morph = pymorphy2.MorphAnalyzer()


def get_normal_text_arr(title, remove_stop_words=True):
    tokens = re.sub("[^а-яА-Яa-zA-ZёЁ]", " ", title).lower().strip().split()
    normal_tokens = [morph.parse(w)[0].normal_form for w in tokens]
    if remove_stop_words:
        words = [w for w in normal_tokens if not w in stopwords.words("russian")]
        return words
    return normal_tokens


def get_normal_text_str(title, remove_stop_words=True):
    return " ".join(get_normal_text_arr(title, remove_stop_words))


def create_batch(df, batch_size):
    num_iter = math.ceil(df.title.shape[0] * 1.0 / batch_size)
    print("number_of_iteration = ", num_iter)
    for i in range(num_iter):
        batch = df[i * batch_size: (i + 1) * batch_size]
        title = batch.title.map(get_normal_text_str).reshape(batch.shape[0], 1)
        pd.DataFrame(
            np.hstack((batch.uid.reshape(batch.shape[0], 1), title.reshape(batch.shape[0], 1))),
            columns=['uid', 'title']).to_csv("batch/batch_" + str(i) + ".csv")
        print("Number of iteration:", i)


def main():
    df = pd.read_csv("ram_train.csv")
    print("Data loaded")
    if df.isnull().values.sum() > 0:
        df = df.dropna()

    create_batch(df, 100000)


if __name__ == '__main__':
    main()
